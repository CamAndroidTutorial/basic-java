package com.bunhann.basic;

import java.util.Arrays;
import java.util.List;

public class ListInterfaceExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> list = Arrays.asList(3, 2, 1, 4, 5, 6, 6);

		// alternative you can declare the list via:
		// List<Integer> list = new ArrayList<>();
		// and use list.add(element); to add elements
		for (Integer integer : list) {
			System.out.println(integer);
		}
	}

}
