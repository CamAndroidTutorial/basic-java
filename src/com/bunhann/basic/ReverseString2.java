package com.bunhann.basic;

public class ReverseString2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String input = "Android Platforms";

		StringBuilder sb = new StringBuilder();

		// append a string into StringBuilder input1
		sb.append(input);

		// reverse StringBuilder input1
		sb = sb.reverse();

		// print reversed String
		System.out.println(sb);
	}

}
