package com.bunhann.basic;

import java.util.Stack;

public class ReverseString4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(reverseString("Android Plateforms"));
	}

	public static String reverseString(String s) {
		Stack<Character> stack = new Stack<Character>();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			stack.push(s.charAt(i));
		}
		while (!stack.empty()) {
			sb.append(stack.pop());
		}
		return sb.toString();

	}

}
